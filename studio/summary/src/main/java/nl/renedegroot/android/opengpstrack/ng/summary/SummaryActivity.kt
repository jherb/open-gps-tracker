package nl.renedegroot.android.opengpstrack.ng.summary

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.View.*
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import nl.renedegroot.android.gpstracker.utils.viewmodel.lazyViewModel
import nl.renedegroot.android.opengpstrack.ng.summary.databinding.SummaryActivitySummaryBinding
import nl.renedegroot.android.opengpstrack.ng.summary.internal.SummaryNavigator

private const val TRACK_URI = "EXTRA_TRACK_URI"

fun startSummaryActivity(context: Context, trackUri: Uri) {
    val intent = Intent(context, SummaryActivity::class.java)
    intent.putExtra(TRACK_URI, trackUri)
    context.startActivity(intent)
}

class SummaryActivity : AppCompatActivity() {

    private val presenter by lazyViewModel { SummaryPresenter() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = DataBindingUtil.setContentView<SummaryActivitySummaryBinding>(this, R.layout.summary__activity_summary)
        setSupportActionBar(binding.summaryToolbar)

        binding.presenter = presenter
        binding.viewModel = presenter.viewModel
        SummaryNavigator(this).observe(presenter.navigation)
        presenter.trackUri = intent.getParcelableExtra(TRACK_URI)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        presenter.onUpNavigation()
        return true
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun onStop() {
        super.onStop()
        presenter.stop()
    }
}
