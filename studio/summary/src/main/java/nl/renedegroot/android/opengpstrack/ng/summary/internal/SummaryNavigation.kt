package nl.renedegroot.android.opengpstrack.ng.summary.internal

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import nl.renedegroot.android.gpstracker.utils.Consumable
import nl.renedegroot.android.gpstracker.utils.consume
import nl.renedegroot.android.opengpstrack.ng.summary.SummaryActivity
import javax.inject.Inject

internal class SummaryNavigation @Inject constructor() {
    private val _action = MutableLiveData<Consumable<Navigation>>()
    val action: LiveData<Consumable<Navigation>>
        get() = _action

    fun stop() {
        _action.postValue(Consumable(Navigation.Stop))
    }
}

internal sealed class Navigation {
    object Stop : Navigation()
}

internal class SummaryNavigator(
        private val activity: SummaryActivity
) {
    fun observe(navigation: SummaryNavigation) {
        navigation.action.observe(activity, consume {
            when (it) {
                Navigation.Stop -> activity.finish()
            }
        })
    }
}
