/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.opengpstrack.ng.summary.summary

import android.content.Context
import android.net.Uri
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import nl.renedegroot.android.gpstracker.service.integration.ContentConstants.Waypoints.WAYPOINTS
import nl.renedegroot.android.gpstracker.utils.contentprovider.append
import nl.renedegroot.android.gpstracker.utils.contentprovider.count
import timber.log.Timber
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.atomic.AtomicInteger

/**
 * Helps in the retrieval, create and keeping up to date of summary data
 */
class SummaryManager(
        private val context: Context,
        private val calculator: SummaryCalculator,
        coroutineScope: CoroutineScope = CoroutineScope(Dispatchers.Default)
) : CoroutineScope by coroutineScope {

    private val summaryCache = ConcurrentHashMap<Uri, Deferred<Summary>>()
    private var activeCount = AtomicInteger(0)

    fun start() {
        activeCount.addAndGet(1)
    }

    fun stop() {
        activeCount.addAndGet(-1)
    }

    /**
     * Collects summary data from the meta table.
     */
    fun collectSummaryInfo(trackUri: Uri,
                           callbackSummary: suspend (Summary) -> Unit) {
        launch {
            val cacheHit = summaryCache[trackUri]
            if (cacheHit != null) {
                val trackWaypointsUri = trackUri.append(WAYPOINTS)
                val trackCount = trackWaypointsUri.count(context.contentResolver)
                val cachedSummary = cacheHit.await()
                if (trackCount == cachedSummary.count) {
                    withContext(Dispatchers.Main) { callbackSummary(cachedSummary) }
                } else {
                    executeTrackCalculation(trackUri, callbackSummary)
                }
            } else {
                executeTrackCalculation(trackUri, callbackSummary)
            }
        }
    }

    private fun executeTrackCalculation(trackUri: Uri, callbackSummary: suspend (Summary) -> Unit) {
        if (activeCount.get() > 0) {
            summaryCache[trackUri] = async {
                Timber.d("Executing track summary calculation for $trackUri")
                val summary = calculator.calculateSummary(trackUri)
                withContext(Dispatchers.Main) { callbackSummary(summary) }
                summary
            }
        }
    }

    fun invalidateCache(trackUri: Uri) {
        summaryCache.remove(trackUri)
    }
}
