/*
 * Open GPS Tracker
 * Copyright (C) 2019 René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses>.
 */

package nl.renedegroot.android.opengpstrack.ng.note

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import nl.renedegroot.android.gpstracker.utils.viewmodel.viewModel
import nl.renedegroot.android.opengpstrack.ng.note.databinding.NotesEditBinding
import nl.renedegroot.android.opengpstrack.ng.note.databinding.NotesShowBinding
import nl.renedegroot.android.opengpstrack.ng.note.internal.NoteManager
import nl.renedegroot.android.opengpstrack.ng.note.internal.NoteNavigator
import nl.renedegroot.android.opengpstrack.ng.note.internal.NotePresenter
import pl.aprilapps.easyphotopicker.DefaultCallback
import pl.aprilapps.easyphotopicker.EasyImage
import java.io.File

private const val WAYPOINT = "ARGUMENT_WAYPOINT"
private const val EDITABLE = "ARGUMENT_EDITABLE"

fun newNoteFragmentInstance(waypoint: Uri, editable: Boolean) = NoteFragment()
        .apply {
            arguments = Bundle().apply {
                putParcelable(WAYPOINT, waypoint)
                putBoolean(EDITABLE, editable)
            }
        }

class NoteFragment : DialogFragment() {

    private val waypoint: Uri
        get() = checkNotNull(requireArguments().getParcelable(WAYPOINT)) as Uri
    private val editable
        get() = requireArguments().getBoolean(EDITABLE)
    private val presenter
        get() = viewModel { NotePresenter(waypoint, NoteManager(requireContext().applicationContext)) }
    private var navigator: NoteNavigator? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = if (editable) {
            val binding = DataBindingUtil.inflate<NotesEditBinding>(inflater, R.layout.notes__edit, container, false)
            binding.presenter = presenter
            binding.model = presenter.model
            binding.root
        } else {
            val binding = DataBindingUtil.inflate<NotesShowBinding>(inflater, R.layout.notes__show, container, false)
            binding.presenter = presenter
            binding.model = presenter.model
            binding.root
        }
        navigator = NoteNavigator(this).apply { observer(viewLifecycleOwner, presenter.navigation.navigation) }

        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        navigator = null
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        EasyImage.handleActivityResult(requestCode, resultCode, data, activity, object : DefaultCallback() {
            override fun onImagesPicked(imageFiles: List<File>, source: EasyImage.ImageSource?, type: Int) {
                presenter.onImagesPicked(imageFiles)
            }
        })
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        navigator?.permissionRequester?.onRequestPermissionsResult(this, requestCode, permissions, grantResults)
    }
}
