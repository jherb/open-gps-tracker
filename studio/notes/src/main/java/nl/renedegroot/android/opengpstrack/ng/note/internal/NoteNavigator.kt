/*
 * Open GPS Tracker
 * Copyright (C) 2019 René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses>.
 */

package nl.renedegroot.android.opengpstrack.ng.note.internal

import android.Manifest
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import nl.renedegroot.android.gpstracker.utils.Consumable
import nl.renedegroot.android.opengpstrack.ng.note.NoteFragment
import nl.renedegroot.android.opengpstrack.ng.note.R
import nl.renedegroot.android.opengpstrack.ng.note.dagger.NotesConfiguration
import pl.aprilapps.easyphotopicker.EasyImage

internal class NoteNavigator(
        private val fragment: NoteFragment
) {

    val permissionRequester = NotesConfiguration.notesComponent.permissionRequester()
    val imagePicker = NotesConfiguration.notesComponent.imagePicker()

    fun observer(owner: LifecycleOwner, navigation: LiveData<Consumable<Navigation>>) {
        navigation.observe(owner, Observer { consumable ->
            consumable.consume { navigate(it) }
        })
    }

    private fun navigate(navigation: Navigation) =
            when (navigation) {
                Navigation.Dismiss -> dismiss()
                Navigation.PickImage -> pickImage()
            }

    private fun dismiss() {
        fragment.dismiss()
    }

    private fun pickImage() {
        permissionRequester.start(fragment, listOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            imagePicker(fragment, R.string.notes__chooser_title)
        }
    }
}
