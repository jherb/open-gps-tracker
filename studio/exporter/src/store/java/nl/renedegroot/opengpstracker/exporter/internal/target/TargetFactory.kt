/*
 * Open GPS Tracker
 * Copyright (C) 2020  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.opengpstracker.exporter.internal.target

import android.content.Context
import nl.renedegroot.android.gpstracker.utils.preference.StringPreferenceDelegate
import nl.renedegroot.opengpstracker.exporter.internal.target.content.ContentTarget
import nl.renedegroot.opengpstracker.exporter.internal.target.drive.GoogleDriveTarget

private const val NAME = "EXPORT_URI"
private const val KEY = "EXPORT_URI"

internal fun createGoogleDriveTarget() = GoogleDriveTarget()

internal fun createContextTarget(context: Context) = ContentTarget(StringPreferenceDelegate(context, NAME, KEY, ""))