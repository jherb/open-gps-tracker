/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.ng.features.activityrecognition

import android.net.Uri
import nl.renedegroot.android.gpstracker.ng.features.dagger.FeatureConfiguration
import nl.renedegroot.android.gpstracker.service.ng.LoggerStateCallback
import javax.inject.Inject

class ActivityRecognizerLoggingBroadcastReceiver : LoggerStateCallback {

    @Inject
    lateinit var activityRecognition: ActivityRecognition

    init {
        FeatureConfiguration.featureComponent.inject(this)
    }

    override fun didStopLogging() {
        activityRecognition.stop()
    }

    override fun didPauseLogging(trackUri: Uri) {
        activityRecognition.stop()
    }

    override fun didStartLogging(trackUri: Uri) {
        activityRecognition.start(trackUri)
    }
}
