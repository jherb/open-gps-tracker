/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.features.track

import android.net.Uri
import nl.renedegroot.android.gpstracker.ng.features.dagger.FeatureConfiguration
import nl.renedegroot.android.gpstracker.ng.features.util.AbstractSelectedTrackPresenter
import nl.renedegroot.android.gpstracker.service.util.readTrackType
import javax.inject.Inject

class TrackPresenter : AbstractSelectedTrackPresenter() {

    @Inject
    lateinit var navigation: TrackNavigation
    val viewModel = TrackViewModel()

    init {
        FeatureConfiguration.featureComponent.inject(this)
    }

    //region Presenter context

    override fun onTrackUpdate(trackUri: Uri?, name: String) {
        if (trackUri != null) {
            viewModel.trackUri.set(trackUri)
            viewModel.name.set(name)
            viewModel.trackIcon.set(trackUri.readTrackType().drawableId)
        }
    }

    //endregion

    //region View callbacks

    fun onListOptionSelected() {
        navigation.showTrackSelection()
    }

    fun onAboutOptionSelected() {
        navigation.showAboutDialog()
    }

    fun onEditOptionSelected() {
        val trackUri = viewModel.trackUri.get()
        trackUri?.let { navigation.showTrackEditDialog(it) }
    }

    fun onGraphsOptionSelected() {
        navigation.showGraphs()
    }

    fun onSettingsOptionSelected() {
        navigation.showSettings()
    }

    //endregion
}
