/*
 * Open GPS Tracker
 * Copyright (C) 2020  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.ng.features.map.editwaypoint

import android.location.Location
import nl.renedegroot.android.gpstracker.ng.base.location.LatLng
import nl.renedegroot.android.gpstracker.service.util.Waypoint
import nl.renedegroot.android.opengpstrack.ng.summary.summary.Summary

fun Summary.findNearestWaypoint(googleLatLng: LatLng): Waypoint? {
    var distance = Float.MAX_VALUE
    var waypoint: Waypoint? = null
    val results = floatArrayOf(0F)

    waypoints.forEach {
        it.forEach { item ->
            Location.distanceBetween(
                    googleLatLng.latitude, googleLatLng.longitude,
                    item.latitude, item.longitude,
                    results
            )
            if (results[0] < distance) {
                distance = results[0]
                waypoint = item
            }
        }
    }

    return waypoint
}
