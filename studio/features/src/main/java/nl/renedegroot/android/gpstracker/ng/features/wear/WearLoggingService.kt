/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.features.wear

import android.app.IntentService
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import nl.renedegroot.android.gpstracker.ng.features.dagger.FeatureConfiguration
import timber.log.Timber
import javax.inject.Inject


class WearLoggingService : IntentService("WearLoggingService") {

    @Inject
    lateinit var statisticsCollector: StatisticsCollector

    private val isWearInstalled by lazy {
        try {
            packageManager.getPackageInfo("com.google.android.wearable.app", PackageManager.GET_META_DATA)
            Timber.v("The Android Wear App is installed")
            true
        } catch (e: PackageManager.NameNotFoundException) {
            Timber.v("The Android Wear App is NOT installed")
            false
        }
    }

    init {
        FeatureConfiguration.featureComponent.inject(this)
    }

    override fun onHandleIntent(intent: Intent?) {
        if (!isWearInstalled) {
            return
        }
        Timber.d("Updating watch status")
        updateTrack(intent)
        updateLoggingState(intent)
    }

    private fun updateLoggingState(intent: Intent?) {
        val state = intent?.getStringExtra(STATE)
        Timber.d("New watch state: $state")
        when (state) {
            START -> statisticsCollector.start()
            PAUSE -> statisticsCollector.pause()
            STOP -> statisticsCollector.stop()
        }
    }

    private fun updateTrack(intent: Intent?) {
        val trackUri: Uri? = intent?.getParcelableExtra(TRACK)
        statisticsCollector.trackUri = trackUri
    }

    companion object {

        private const val TRACK = "WearLoggingService_Track"
        private const val STATE = "WearLoggingService_State"
        private const val START = "WearLoggingService_State_Started"
        private const val PAUSE = "WearLoggingService_State_Paused"
        private const val STOP = "WearLoggingService_State_Stopped"

        @JvmStatic
        fun startStartedIntent(context: Context, trackUri: Uri) {
            val intent = Intent(context, WearLoggingService::class.java)
            intent.putExtra(TRACK, trackUri)
            intent.putExtra(STATE, START)
            context.startService(intent)
        }

        @JvmStatic
        fun startPausedIntent(context: Context, trackUri: Uri) {
            val intent = Intent(context, WearLoggingService::class.java)
            intent.putExtra(TRACK, trackUri)
            intent.putExtra(STATE, PAUSE)
            context.startService(intent)
        }

        @JvmStatic
        fun startStoppedIntent(context: Context) {
            val intent = Intent(context, WearLoggingService::class.java)
            intent.putExtra(STATE, STOP)
            context.startService(intent)
        }
    }
}
