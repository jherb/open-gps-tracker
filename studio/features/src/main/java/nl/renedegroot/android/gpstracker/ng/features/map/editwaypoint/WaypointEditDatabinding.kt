/*
 * Open GPS Tracker
 * Copyright (C) 2020  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.ng.features.map.editwaypoint

import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import nl.renedegroot.android.gpstracker.ng.features.dagger.FeatureConfiguration
import nl.renedegroot.android.gpstracker.ng.features.map.util.onMap
import nl.renedegroot.android.gpstracker.service.util.Waypoint
import nl.renedegroot.android.opengpstrack.ng.features.R

@BindingAdapter("text")
fun TextView.setWaypoint(selection: WaypointSelection?) {
    val waypoint = selection?.waypoint
    val formatter = FeatureConfiguration.featureComponent.statisticsFormatter()
    val dash = { context.getString(R.string.empty_dash) }

    text = if (waypoint != null) {
        val time = if (waypoint.time != 0L) {
            formatter.convertTimestampToTime(context, waypoint.time)
        } else {
            dash()
        }
        val speed = if (waypoint.speed != 0.0) {
            formatter.convertMeterPerSecondsToSpeed(context, waypoint.speed.toFloat(), isRunning = false, decimals = false)
        } else {
            dash()
        }
        val altitude = if (waypoint.altitude != 0.0) {
            formatter.convertMetersAltitude(context, waypoint.altitude.toFloat())
        } else {
            dash()
        }
        context.getString(R.string.waypoint_describtion, time, speed, altitude)
    } else {
        dash()
    }
}


@BindingAdapter("waypointMarker")
fun setWaypointMarker(mapView: MapView, oldWaypoint: WaypointSelection?, newWaypoint: WaypointSelection?) {
    val addMarker: (GoogleMap) -> Unit = { googleMap ->
        oldWaypoint?.removeFromMap()
        newWaypoint?.addToMap(googleMap)
    }
    mapView.onMap(addMarker)
}