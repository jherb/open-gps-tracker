/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.features.map.rendering

import androidx.annotation.VisibleForTesting

import com.google.android.gms.maps.model.LatLngBounds

import nl.renedegroot.android.gpstracker.ng.base.location.LatLng
import nl.renedegroot.android.gpstracker.service.util.Waypoint

private const val MAX_PIXEL_COORDINATE = 0.9999
private const val MIN_PIXEL_COORDINATE = -0.9999

/**
 * Reworked code found on OpenStreetMap Wiki and Stack Overflow
 *
 * @see [openstreetmap](https://wiki.openstreetmap.org/wiki/Slippy_map_tilenames)
 *
 * @see [stackoverflow](https://stackoverflow.com/questions/20382823/google-maps-api-v2-draw-part-of-circle-on-mapfragment)
 */
internal class TileProjection(@get:VisibleForTesting
                              internal val tileSize: Float) {

    private val origin: Point = Point((tileSize / 2.0f).toDouble(), (tileSize / 2.0f).toDouble())
    private val pixelsPerLonRadian: Double = tileSize / (2 * Math.PI)
    private val pixelsPerLonDegree: Double = tileSize / 360.0

    /**
     * Stack overflow projection code from Latitude/Longitude to tile pixel coordinates
     *
     * @param latLng    in parameter
     * @param tilePoint out parameter with the result
     */
    fun latLngToPoint(latLng: LatLng, tilePoint: Point, x: Int, y: Int, zoom: Int) {
        latLngToWorldCoordinates(latLng, tilePoint)
        worldToTileCoordinates(tilePoint, tilePoint, x, y, zoom)
    }

    /**
     * Stack overflow projection code from Latitude/Longitude to tile pixel coordinates
     *
     * @param latLng
     * @param worldPoint out parameter with the result
     */
    fun latLngToWorldCoordinates(latLng: Waypoint, worldPoint: Point) {
        worldPoint.x = origin.x + latLng.longitude * pixelsPerLonDegree
        val sinY = bound(Math.sin(Math.toRadians(latLng.latitude)))
        worldPoint.y = origin.y + 0.5f * (Math.log((1 + sinY) / (1 - sinY)) * -pixelsPerLonRadian)
    }

    fun latLngToWorldCoordinates(latLng: LatLng, worldPoint: Point) {
        worldPoint.x = origin.x + latLng.longitude * pixelsPerLonDegree
        val sinY = bound(Math.sin(Math.toRadians(latLng.latitude)))
        worldPoint.y = origin.y + 0.5f * (Math.log((1 + sinY) / (1 - sinY)) * -pixelsPerLonRadian)
    }

    /**
     * Stack overflow projection code from Latitude/Longitude to tile pixel coordinates
     *
     * @param worldPoint
     * @param tilePoint  out parameter with the result
     */
    fun worldToTileCoordinates(worldPoint: Point, tilePoint: Point, x: Int, y: Int, zoom: Int) {
        val numTiles = 1 shl zoom
        tilePoint.x = worldPoint.x * numTiles
        tilePoint.y = worldPoint.y * numTiles
        tilePoint.x -= (x * tileSize).toDouble()
        tilePoint.y -= (y * tileSize).toDouble()
    }

    /**
     * Stack overflow projection code from Latitude/Longitude to tile pixel coordinates
     *
     * @param value
     * @return value bound between min and max
     */
    private fun bound(value: Double): Double =
            value.coerceAtLeast(MIN_PIXEL_COORDINATE).coerceAtMost(MAX_PIXEL_COORDINATE)

    /**
     * Based on work from: https://wiki.openstreetmap.org/wiki/Slippy_map_tilenames
     *
     * @param y tile coordinate system Y
     * @param z tile coordinate system zoom
     * @return latitude
     * @license Creative Commons Attribution-ShareAlike 2.0 license
     */
    private fun tileToLatitude(y: Int, z: Int): Double {
        val n = Math.PI - 2.0 * Math.PI * y.toDouble() / Math.pow(2.0, z.toDouble())
        return Math.toDegrees(Math.atan(Math.sinh(n)))
    }

    /**
     * Based on work from: https://wiki.openstreetmap.org/wiki/Slippy_map_tilenames
     *
     * @param x tile coordinate system X
     * @param z tile coordinate system zoom
     * @return longitude
     * @license Creative Commons Attribution-ShareAlike 2.0 license
     */
    private fun tileToLongitude(x: Int, z: Int): Double {
        return x / Math.pow(2.0, z.toDouble()) * 360.0 - 180.0
    }

    /**
     * Builds a latitude/longitude bounding box for a tile
     *
     *
     * Based on work from: https://wiki.openstreetmap.org/wiki/Slippy_map_tilenames
     *
     * @param x    tile coordinate system X
     * @param y    tile coordinate system Y
     * @param zoom tile coordinate system zoom
     * @return
     * @license Creative Commons Attribution-ShareAlike 2.0 license
     */
    fun tileBounds(x: Int, y: Int, zoom: Int): LatLngBounds {
        val builder = LatLngBounds.Builder()
        val (latitude, longitude) = LatLng(tileToLatitude(y, zoom), tileToLongitude(x, zoom))
        val (latitude1, longitude1) = LatLng(tileToLatitude(y + 1, zoom), tileToLongitude(x + 1, zoom))
        builder.include(com.google.android.gms.maps.model.LatLng(latitude, longitude))
                .include(com.google.android.gms.maps.model.LatLng(latitude1, longitude1))

        return builder.build()
    }
}
