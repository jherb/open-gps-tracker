/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.ng.features.graphs.dataproviders

import android.content.Context
import androidx.annotation.WorkerThread
import nl.renedegroot.android.gpstracker.ng.features.dagger.FeatureConfiguration
import nl.renedegroot.android.gpstracker.ng.features.graphs.GraphSpeedConverter
import nl.renedegroot.android.gpstracker.ng.features.graphs.widgets.GraphPoint
import nl.renedegroot.android.opengpstrack.ng.summary.summary.Summary
import nl.renedegroot.android.gpstracker.utils.formatting.StatisticsFormatter
import nl.renedegroot.android.gpstracker.utils.preference.UnitSettingsPreferences
import nl.renedegroot.android.opengpstrack.ng.features.R
import javax.inject.Inject

class AltitudeOverTimeDataProvider : GraphDataCalculator {

    @Inject
    lateinit var unitSettingsPreferences: UnitSettingsPreferences

    @Inject
    lateinit var statisticsFormatter: StatisticsFormatter

    @Inject
    lateinit var graphSpeedConverter: GraphSpeedConverter

    @Inject
    lateinit var rangePicker: AltitudeRangePicker

    private var _graphPoint: List<GraphPoint> = emptyList()

    override val graphPoint: List<GraphPoint>
        get() = _graphPoint

    override val yLabel: Int
        get() = R.string.graph_label_height

    override val xLabel: Int
        get() = R.string.graph_label_time

    init {
        FeatureConfiguration.featureComponent.inject(this)
    }

    override fun prettyMinYValue(yValue: Float) =
            rangePicker.prettyMinYValue(yValue)

    override fun prettyMaxYValue(yValue: Float) =
            rangePicker.prettyMaxYValue(yValue)

    @WorkerThread
    override fun calculateGraphPoints(summary: Summary) {
        val graphPoints = mutableListOf<GraphPoint>()
        val baseTime = summary.deltas.firstOrNull()?.firstOrNull()?.totalMilliseconds ?: 0L
        summary.deltas.forEach {
            addSegmentToGraphPoints(it, baseTime, graphPoints)
        }
        _graphPoint = graphPoints
    }

    override fun describeYvalue(context: Context, yValue: Float): String {
        return statisticsFormatter.convertMetersAltitude(context, yValue)
    }

    override fun describeXvalue(context: Context, xValue: Float): String {
        return statisticsFormatter.convertSpanDescriptiveDuration(context, xValue.toLong())
    }

    private fun addSegmentToGraphPoints(deltas: List<Summary.Delta>, baseTime: Long, graphPoints: MutableList<GraphPoint>) {
        fun Long.toX() = (this - baseTime).toFloat()
        deltas.forEach {
            if (it.altitude >= 0F) {
                val moment = it.totalMilliseconds - (it.deltaMilliseconds / 2L)
                graphPoints.add(GraphPoint(moment.toX(), it.altitude.toFloat()))
            }
        }
    }
}

