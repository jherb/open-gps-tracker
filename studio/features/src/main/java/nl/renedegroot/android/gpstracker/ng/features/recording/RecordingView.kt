/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.features.recording

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import android.net.Uri
import nl.renedegroot.android.gpstracker.ng.features.util.SingleLiveEvent
import nl.renedegroot.android.opengpstrack.ng.features.R

class RecordingView(uri: Uri?) {
    val trackUri = ObservableField(uri)
    val isRecording = ObservableBoolean(false)
    val state = ObservableField<Int>(R.string.empty_dash)
    val name = ObservableField<String?>()
    val summary = ObservableField<SummaryText>()
    val maxSatellites = ObservableInt(0)
    val currentSatellites = ObservableInt(0)
    val isScanning = ObservableBoolean(false)
    val hasFix = ObservableBoolean(false)
    val signalQuality = ObservableInt(0)

    val navigation = SingleLiveEvent<Navigation>()

    object SignalQualityLevel {
        const val none = 0
        const val low = 1
        const val medium = 2
        const val high = 3
        const val excellent = 4
    }
}

data class SummaryText(val string: Int, val meterPerSecond: Float, val isRunners: Boolean, val meters: Float, val msDuration: Long)
