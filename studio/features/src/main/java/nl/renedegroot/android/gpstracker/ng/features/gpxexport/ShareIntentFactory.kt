/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.features.gpxexport

import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.FileProvider.getUriForFile
import androidx.core.os.bundleOf
import androidx.core.view.drawToBitmap
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentActivity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import nl.renedegroot.android.gpstracker.ng.base.BuildConfig
import nl.renedegroot.android.gpstracker.ng.features.dagger.FeatureConfiguration
import nl.renedegroot.android.gpstracker.ng.features.tracklist.TrackViewState
import nl.renedegroot.android.gpstracker.utils.file.cacheFile
import nl.renedegroot.android.gpstracker.utils.formatting.StatisticsFormatter
import nl.renedegroot.android.opengpstrack.ng.features.R
import nl.renedegroot.android.opengpstrack.ng.features.databinding.ItemShareBinding
import nl.renedegroot.android.opengpstrack.ng.summary.summary.SummaryManager
import nl.renedegroot.opengpstracker.exporter.GpxCreator
import nl.renedegroot.opengpstracker.exporter.GpxCreator.MIME_TYPE_GPX
import java.io.File
import java.io.FileOutputStream
import javax.inject.Inject

const val MIME_TYPE_GENERAL = "application/octet-stream"
const val MIME_TYPE_GPX = "application/gpx+xml"
const val MIME_TYPE_JPG = "image/jpeg"
const val MIME_TYPE_ANY = "*/*"
const val AUTHORITY = BuildConfig.providerAuthority + ".gpxshareprovider"

private const val DIALOG_TAG = "TAG_SHARE_DIALOG"

class ShareIntentFactory @Inject constructor(
        private val context: Context,
        private val resolver: ContentResolver) {

    fun createGpxShareIntent(activity: FragmentActivity, trackUri: Uri) {
        GlobalScope.launch(Dispatchers.Default) {
            val exportFile = streamForTrackShare(activity, trackUri, "gpx")
            val outputStream = FileOutputStream(exportFile)
            GpxCreator(resolver, trackUri).createGpx(outputStream)

            val contentUri = getUriForFile(context, AUTHORITY, exportFile)
            val shareIntent = Intent().apply {
                action = Intent.ACTION_SEND
                data = contentUri
                type = MIME_TYPE_GPX
                putExtra(Intent.EXTRA_STREAM, contentUri)
            }

            activity.startActivity(Intent.createChooser(shareIntent, activity.getString(R.string.track_share)))
        }
    }

    fun createSocialShareIntent(context: FragmentActivity, trackUri: Uri) {
        createShareDialog(trackUri)
                .show(context.supportFragmentManager, DIALOG_TAG)
    }
}

private fun streamForTrackShare(context: Context, trackUri: Uri, extension: String): File {
    val fileName = GpxCreator.fileName(trackUri, extension)
    return context.cacheFile("sharing", fileName)
}

private const val ARG_TRACK_URI = "ARG_TRACK_URI"

private fun createShareDialog(trackUri: Uri) = ShareDialog().apply {
    arguments = bundleOf(ARG_TRACK_URI to trackUri)
}

class ShareDialog : DialogFragment() {

    @Inject
    lateinit var formatter: StatisticsFormatter

    @Inject
    lateinit var summaryManager: SummaryManager

    init {
        FeatureConfiguration.featureComponent.inject(this)
    }

    private val trackUri: Uri
        get() = checkNotNull(requireArguments().getParcelable(ARG_TRACK_URI))

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return DataBindingUtil.inflate<ItemShareBinding>(inflater, R.layout.item_share, container, false).apply {
            rowTrackMap.onCreate(null)
            viewState = TrackViewState(trackUri)
            presenter = SharePresenter(this@ShareDialog, itemShareRoot, trackUri)
            summaryManager.collectSummaryInfo(trackUri) {
                viewState?.setSummary(requireContext(), formatter, it)
            }
        }.root
    }
}

class SharePresenter(
        private val dialog: ShareDialog,
        private val view: View,
        private val trackUri: Uri
) {

    private var isSharing = false

    fun onShare() {
        if (isSharing) return
        isSharing = true

        GlobalScope.launch(Dispatchers.Default) {
            val bitmap = view.drawToBitmap()

            val activity = dialog.requireActivity()
            dialog.dismiss()

            val exportFile = streamForTrackShare(activity, trackUri, "jpg")
            val outputStream = FileOutputStream(exportFile)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream)

            val contentUri = getUriForFile(activity, AUTHORITY, exportFile)
            val shareIntent = Intent().apply {
                action = Intent.ACTION_SEND
                data = contentUri
                type = MIME_TYPE_JPG
                putExtra(Intent.EXTRA_STREAM, contentUri)
            }

            activity.startActivity(Intent.createChooser(shareIntent, activity.getString(R.string.track_share)))
        }
    }
}
