/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.ng.features.recording

import android.net.Uri
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import nl.renedegroot.android.gpstracker.ng.base.common.controllers.content.ContentController
import nl.renedegroot.android.gpstracker.ng.base.common.controllers.gpsstatus.GpsStatusController
import nl.renedegroot.android.gpstracker.ng.features.recording.RecordingView.SignalQualityLevel.excellent
import nl.renedegroot.android.gpstracker.ng.features.recording.RecordingView.SignalQualityLevel.high
import nl.renedegroot.android.gpstracker.ng.features.recording.RecordingView.SignalQualityLevel.low
import nl.renedegroot.android.gpstracker.ng.features.recording.RecordingView.SignalQualityLevel.medium
import nl.renedegroot.android.gpstracker.ng.features.recording.RecordingView.SignalQualityLevel.none
import nl.renedegroot.android.gpstracker.ng.features.util.ImmediateExecutor
import nl.renedegroot.android.gpstracker.ng.features.util.MockBaseComponentTestRule
import nl.renedegroot.android.gpstracker.service.integration.ServiceConstants.STATE_LOGGING
import nl.renedegroot.android.gpstracker.service.integration.ServiceConstants.STATE_PAUSED
import nl.renedegroot.android.gpstracker.service.integration.ServiceConstants.STATE_STOPPED
import nl.renedegroot.android.gpstracker.service.util.LoggingStateController
import nl.renedegroot.android.opengpstrack.ng.summary.summary.SummaryManager
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.nullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.reset
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnit

class RecordingPresenterTest {

    lateinit var sut: RecordingPresenter

    @get:Rule
    var BaseComponentRule = MockBaseComponentTestRule()

    @get:Rule
    var mockitoRule = MockitoJUnit.rule()

    @Mock
    lateinit var uri: Uri

    @Mock
    lateinit var contentController: ContentController

    @Mock
    lateinit var gpsStatusController: GpsStatusController

    @Mock
    lateinit var summaryManager: SummaryManager

    @Mock
    lateinit var loggingStateController: LoggingStateController

    @Before
    fun setUp() {
        Dispatchers.setMain(ImmediateExecutor().asCoroutineDispatcher())

        sut = RecordingPresenter()
        sut.loggingStateController = loggingStateController
        sut.summaryManager = summaryManager
        sut.contentController = contentController
        sut.gpsStatusController = gpsStatusController
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun testStop() {
        // Arrange
        sut.start()
        loggingStateTo(STATE_LOGGING)
        reset(contentController)
        reset(gpsStatusController)
        // Act
        sut.stop()
        // Assert
        verify(contentController).unregisterObserver()
        verify(gpsStatusController).stopUpdates()
    }

    @Test
    fun testConnectToLoggingService() {
        // Arrange
        sut.start()
        // Act
        loggingStateTo(STATE_LOGGING)
        // Assert
        Assert.assertThat(sut.viewModel.isRecording.get(), `is`(true))
        Assert.assertThat(sut.viewModel.trackUri.get(), `is`(uri))
    }

    @Test
    fun testConnectToPauseService() {
        // Arrange
        sut.start()
        // Act
        loggingStateTo(STATE_PAUSED)
        // Assert
        Assert.assertThat(sut.viewModel.isRecording.get(), `is`(true))
        Assert.assertThat(sut.viewModel.trackUri.get(), `is`(uri))
    }

    @Test
    fun testConnectToStoppedService() {
        // Arrange
        sut.start()
        // Act
        loggingStateTo(STATE_STOPPED)
        // Assert
        Assert.assertThat(sut.viewModel.isRecording.get(), `is`(false))
        Assert.assertThat(sut.viewModel.trackUri.get(), `is`(nullValue()))
    }

    @Test
    fun testChangeToLoggingService() {
        // Arrange
        sut.start()
        // Act
        loggingStateTo(STATE_STOPPED)
        // Assert
        Assert.assertThat(sut.viewModel.isRecording.get(), `is`(false))
        Assert.assertThat(sut.viewModel.trackUri.get(), `is`(nullValue()))
    }

    @Test
    fun testGpsStart() {
        // Act
        sut.onStartListening()
        // Assert
        assertThat(sut.viewModel.isScanning.get(), `is`(true))
        assertThat(sut.viewModel.hasFix.get(), `is`(false))
    }

    @Test
    fun testGpsStop() {
        // Act
        sut.onStopListening()
        // Assert
        assertThat(sut.viewModel.isScanning.get(), `is`(false))
        assertThat(sut.viewModel.hasFix.get(), `is`(false))
        assertThat(sut.viewModel.maxSatellites.get(), `is`(0))
        assertThat(sut.viewModel.currentSatellites.get(), `is`(0))
    }

    @Test
    fun onFirstFix() {
        // Act
        sut.onFirstFix()
        // Assert
        assertThat(sut.viewModel.hasFix.get(), `is`(true))
        assertThat(sut.viewModel.signalQuality.get(), `is`(4))
    }

    @Test
    fun onNoSignal() {
        // Act
        sut.onChange(0, 0)
        // Assert
        assertThat(sut.viewModel.maxSatellites.get(), `is`(0))
        assertThat(sut.viewModel.currentSatellites.get(), `is`(0))
        assertThat(sut.viewModel.signalQuality.get(), `is`(none))
    }

    @Test
    fun onLowSignal() {
        // Act
        sut.onChange(4, 4)
        // Assert
        assertThat(sut.viewModel.maxSatellites.get(), `is`(4))
        assertThat(sut.viewModel.currentSatellites.get(), `is`(4))
        assertThat(sut.viewModel.signalQuality.get(), `is`(low))
    }

    @Test
    fun onMediumSignal() {
        // Act
        sut.onChange(6, 20)
        // Assert
        assertThat(sut.viewModel.maxSatellites.get(), `is`(20))
        assertThat(sut.viewModel.currentSatellites.get(), `is`(6))
        assertThat(sut.viewModel.signalQuality.get(), `is`(medium))
    }

    @Test
    fun onHighSignal() {
        // Act
        sut.onChange(8, 20)
        // Assert
        assertThat(sut.viewModel.signalQuality.get(), `is`(high))
    }

    @Test
    fun onExcellentSignal() {
        // Act
        sut.onChange(10, 20)
        // Assert
        assertThat(sut.viewModel.signalQuality.get(), `is`(excellent))
    }

    private fun loggingStateTo(state: Int) {
        `when`(loggingStateController.loggingState).thenReturn(state)
        `when`(loggingStateController.trackUri).thenReturn(uri)
        when (state) {
            STATE_STOPPED -> sut.didStopLogging()
            STATE_PAUSED -> sut.didPauseLogging(uri)
            STATE_LOGGING -> sut.didStartLogging(uri)
        }
    }
}
