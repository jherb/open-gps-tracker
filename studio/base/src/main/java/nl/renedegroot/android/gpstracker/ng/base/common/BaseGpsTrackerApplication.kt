/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.base.common

import android.app.Application
import android.os.StrictMode
import androidx.annotation.CallSuper
import com.facebook.stetho.Stetho
import com.google.firebase.FirebaseApp
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.crashlytics.FirebaseCrashlytics
import nl.renedegroot.android.gpstracker.ng.base.BuildConfig
import timber.log.Timber


/**
 * Start app generic services
 */
open class BaseGpsTrackerApplication : Application() {

    private var logcatLogging = BuildConfig.DEBUG
    private var installStetho = BuildConfig.DEBUG
    private var installCrashlytics = BuildConfig.FLAVOR == "store" && !BuildConfig.DEBUG
    private var installAnalytics = BuildConfig.FLAVOR == "store" && !BuildConfig.DEBUG

    @CallSuper
    override fun onCreate() {
        super.onCreate()
        setupLogging()
        setupFirebase()
        setupStetho()
    }

    private fun setupStetho() {
        if (installStetho) {
            ofMainThread {
                Stetho.initializeWithDefaults(this)
            }
        }
    }

    private fun setupFirebase() {
        ofMainThread {
            FirebaseApp.initializeApp(this)
            FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(installCrashlytics)
            FirebaseAnalytics.getInstance(this).setAnalyticsCollectionEnabled(installAnalytics)
        }
    }

    protected fun setupLogging() {
        if (logcatLogging) {
            Timber.plant(Timber.DebugTree())
//            if (StrictMode.ThreadPolicy.Builder().build() != null) {
//                StrictMode.setThreadPolicy(
//                        StrictMode.ThreadPolicy.Builder()
//                                .detectAll()
//                                .penaltyDeath()
//                                .build())
//            }
        }
    }
}
