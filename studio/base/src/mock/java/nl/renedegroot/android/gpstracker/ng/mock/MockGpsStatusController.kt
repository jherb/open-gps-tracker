/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.ng.mock

import android.os.Handler
import android.os.Looper
import nl.renedegroot.android.gpstracker.ng.base.common.controllers.gpsstatus.GpsStatusController

typealias Action = (GpsStatusController.Listener) -> Unit

class MockGpsStatusController : GpsStatusController {
    private var listener: GpsStatusController.Listener? = null
    private val commands = listOf<Action>(
            { it.onStartListening() },
            { it.onChange(0, 0) },
            { it.onChange(0, 8) },
            { it.onChange(1, 10) },
            { it.onChange(3, 12) },
            { it.onChange(5, 11) },
            { it.onChange(7, 14) },
            { it.onChange(9, 21) },
            { it.onFirstFix() },
            { it.onStopListening() }
    )

    private var handler: Handler? = null

    override fun startUpdates(listener: GpsStatusController.Listener) {
        this.listener = listener
        handler = Handler(Looper.getMainLooper())
        nextCommand(0)
    }

    override fun stopUpdates() {
        handler = null
    }

    private fun scheduleNextCommand(i: Int) {
        handler?.postDelayed({ nextCommand(i) }, 1500)
    }

    private fun nextCommand(i: Int) {
        listener?.let { listener -> commands[i](listener) }
        val next = if (i < (commands.count() - 1)) i + 1 else 0
        scheduleNextCommand(next)
    }
}
