/*
 * Open GPS Tracker
 * Copyright (C) 2019 René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses>.
 */

package nl.renedegroot.android.gpstracker.service.ng.internal

import android.content.Intent
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import nl.renedegroot.android.gpstracker.service.ng.COMMAND_INTERVAL
import nl.renedegroot.android.gpstracker.service.ng.COMMAND_PAUSE
import nl.renedegroot.android.gpstracker.service.ng.COMMAND_RESUME
import nl.renedegroot.android.gpstracker.service.ng.COMMAND_START
import nl.renedegroot.android.gpstracker.service.ng.COMMAND_STOP
import nl.renedegroot.android.gpstracker.service.ng.EXTRA_COMMAND
import nl.renedegroot.android.gpstracker.service.ng.EXTRA_INTERVAL
import nl.renedegroot.android.gpstracker.service.ng.EXTRA_NAME
import nl.renedegroot.android.gpstracker.utils.preference.LoggingInterval
import nl.renedegroot.android.gpstracker.utils.preference.SETTINGS_CUSTOM_INTERVAL_PERIOD_DEFAULT
import timber.log.Timber
import java.util.concurrent.CountDownLatch

internal class GpsExecutor(
        private val gpsLogger: GpsLogger
) {

    private val startLatch = CountDownLatch(1)

    internal suspend fun start() {
        if (startLatch.count == 0L) return

        withContext(Dispatchers.IO) {
            gpsLogger.loadState()
        }
        startLatch.countDown()
    }

    suspend fun command(intent: Intent) {
        try {
            startLatch.await()
        } catch (e: InterruptedException) {
            return
        }
        execute(intent)
        withContext(Dispatchers.IO) {
            gpsLogger.saveState()
        }
    }

    private fun execute(intent: Intent) {
        if (intent.hasExtra(EXTRA_COMMAND)) {
            val command = intent.getStringExtra(EXTRA_COMMAND)
            Timber.d("Executing command in intent $command")
            when (command) {
                COMMAND_START -> gpsLogger.startLogging(intent.getStringExtra(EXTRA_NAME), intent.getIntExtra(EXTRA_INTERVAL, SETTINGS_CUSTOM_INTERVAL_PERIOD_DEFAULT))
                COMMAND_PAUSE -> gpsLogger.pauseLogging()
                COMMAND_RESUME -> gpsLogger.resumeLogging()
                COMMAND_STOP -> gpsLogger.stopLogging()
                COMMAND_INTERVAL -> gpsLogger.updateInterval(intent.getIntExtra(EXTRA_INTERVAL, SETTINGS_CUSTOM_INTERVAL_PERIOD_DEFAULT))
                else -> Timber.e("Unknown command in intent $intent")
            }
        } else {
            Timber.e("Missing command in intent ${intent.extras}")
        }
    }
}
