/*
 * Open GPS Tracker
 * Copyright (C) 2019 René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses>.
 */

package nl.renedegroot.android.gpstracker.service.ng

import android.app.Service
import android.content.Intent
import android.os.IBinder
import dagger.Lazy
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import nl.renedegroot.android.gpstracker.service.dagger.ComponentFactory
import nl.renedegroot.android.gpstracker.service.ng.internal.GpsExecutor
import nl.renedegroot.android.gpstracker.service.ng.internal.GpsLogger
import nl.renedegroot.android.gpstracker.service.ng.internal.GpsLoggerBinder
import timber.log.Timber
import javax.inject.Inject

class GpsLoggerService(
        private val coroutineScope: CoroutineScope = CoroutineScope(Dispatchers.Default)
) : Service(), CoroutineScope by coroutineScope {

    @Inject
    internal lateinit var gpsExecutor: Lazy<GpsExecutor>
    @Inject
    internal lateinit var gpsLogger: Lazy<GpsLogger>

    init {
        ComponentFactory().createServiceComponent(this)
                .inject(this)
    }

    override fun onCreate() {
        super.onCreate()
        Timber.d("Create Service")
        launch {
            gpsExecutor.get().start()
        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Timber.d("onStartCommand $intent")
        launch {
            if (intent != null) {
                gpsExecutor.get().command(intent)
            }
        }
        return START_STICKY
    }

    override fun onDestroy() {
        super.onDestroy()
        Timber.d("Destroy Service")
        cancel()
    }

    override fun onBind(intent: Intent?): IBinder = GpsLoggerBinder(gpsLogger.get())
}

