/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.service.util

import android.net.Uri
import com.google.android.gms.maps.model.LatLngBounds
import nl.renedegroot.android.gpstracker.ng.base.location.LatLng

class DefaultResultHandler : ResultHandler {

    private val segmentsBuilder = mutableListOf<MutableList<Waypoint>>()
    private var boundsBuilder: LatLngBounds.Builder? = null

    var uri: Uri? = null
    var name: String? = null
    val bounds: LatLngBounds by lazy {
        val builder = boundsBuilder
        if (builder != null) {
            builder.build()
        } else {
            LatLngBounds(com.google.android.gms.maps.model.LatLng(0.0, 0.0), com.google.android.gms.maps.model.LatLng(50.0, 5.0))
        }
    }
    val waypoints: List<List<Waypoint>> by lazy {
        segmentsBuilder
        segmentsBuilder.filter {
            it.count() >= 1
        }
    }

    override fun setTrack(uri: Uri, name: String) {
        this.uri = uri
        this.name = name
    }

    override fun addSegment() {
        segmentsBuilder.add(mutableListOf())
    }

    override fun addWaypoint(waypoint: Waypoint) {
        // Add each waypoint to the end of the last list of points (the current segment)
        segmentsBuilder.last().add(waypoint)
        // Build a bounds for the whole track
        boundsBuilder = boundsBuilder ?: LatLngBounds.Builder()
        boundsBuilder?.include(waypoint.asGoogleLatLng())
    }
}

fun Waypoint.asGoogleLatLng(): com.google.android.gms.maps.model.LatLng {
    return com.google.android.gms.maps.model.LatLng(this.latitude, this.longitude)
}
